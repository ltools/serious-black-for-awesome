Serious Black for Awesome
=========================

While it sounds cooler than "Severus Snape for President,"
it is actually just a theme for the Awesome Window Manager,
in the same plain dark style as my other theme, "Serious Black"
that was designed for Fluxbox.
