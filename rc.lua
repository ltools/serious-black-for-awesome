require("awful")
require("awful.autofocus")
require("awful.rules")
require("beautiful")
require("naughty")
require("debian.menu")

awful.util.spawn_with_shell("xcompmgr &")
beautiful.init("/home/" .. os.getenv("USER") .. "/.config/awesome/theme.lua")

-- Section: Default applications
terminal = "urxvt"
editor = "vim"
editor_def = os.getenv("EDITOR") or "editor"
editor_cmd = terminal .. " -e " .. editor
modkey = "Mod4"

-- Section: Tags and layouts
layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.floating
}
tags = {}
for s = 1, screen.count() do
    tags[s] = awful.tag(
        {"1:console", "2:work", "3:web", "4:media", "5:misc"},
        s,
        layouts[1]
    )
end

-- Section: Wibox -> Menus
myawesomemenu = {
    {"restart", awesome.restart},
    {"quit", awesome.quit}
}
mymainmenu = awful.menu({items = {
    {"awesome", myawesomemenu, beautiful.awesome_icon},
    {"Debian", debian.menu.Debian_menu.Debian},
    {"open terminal", terminal}
}})
mylauncher = awful.widget.launcher({
    image = image(beautiful.awesome_icon),
    menu = mymainmenu
})

-- Section: Wibox -> Tags, clock, tray
mytextclock = awful.widget.textclock({align = "right"})
mysystray = widget({type = "systray"})
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
    awful.button({modkey}, 1, awful.client.movetotag),
    awful.button({modkey}, 3, awful.client.toggletag),
    awful.button({}, 1, awful.tag.viewonly),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
)

-- Section: Wibox -> Tasklist
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
    awful.button({}, 1,
        function(c)
            if not c:isvisible() then
                awful.tag.viewonly(c:tags()[1])
            end
            client.focus = c
            c:raise()
        end
    ),
    awful.button({}, 3,
        function()
            if instance then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients({ width=250 })
            end
        end
    ),
    awful.button({}, 4,
        function()
            awful.client.focus.byidx(1)
            if client.focus then client.focus:raise() end
        end
    ),
    awful.button({}, 5,
        function()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end
    )
)

-- Section: Wibox -> Complete wibox
for s = 1, screen.count() do
    mypromptbox[s] = awful.widget.prompt({
        layout = awful.widget.layout.horizontal.leftright
    })
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
        awful.button({}, 1, function() awful.layout.inc(layouts,  1) end),
        awful.button({}, 3, function() awful.layout.inc(layouts, -1) end),
        awful.button({}, 4, function() awful.layout.inc(layouts,  1) end),
        awful.button({}, 5, function() awful.layout.inc(layouts, -1) end))
    )
    mytaglist[s] = awful.widget.taglist(
        s,
        awful.widget.taglist.label.all,
        mytaglist.buttons
    )
    mytasklist[s] = awful.widget.tasklist(
        function(c)
            return awful.widget.tasklist.label.currenttags(c, s)
        end,
        mytasklist.buttons
    )
    mywibox = {}
    mywibox[s] = awful.wibox({position = "bottom", screen = s})
    mywibox[s].widgets = {
        {
            mytaglist[s],
            mypromptbox[s],
            layout = awful.widget.layout.horizontal.leftright
        },
        mytextclock,
        layout = awful.widget.layout.horizontal.rightleft
    }
end

-- Section: Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({}, 3, function() mymainmenu:toggle() end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
))

-- Section: Keyboard shortcuts -> Global keys
globalkeys = awful.util.table.join(
    awful.key({modkey, "Control"}, "space", awful.client.floating.toggle),
    awful.key({modkey, "Control"}, "r", awesome.restart),
    awful.key({modkey, "Shift"}, "q", awesome.quit),
    awful.key({modkey, "Shift"}, "Tab", awful.tag.viewprev),
    awful.key({modkey}, "Tab", awful.tag.viewnext),
    awful.key({modkey}, "Escape", awful.tag.history.restore),
    awful.key({modkey}, "u", awful.client.urgent.jumpto),
    awful.key({modkey}, "o", awful.client.movetoscreen),
    awful.key({modkey}, "j",
        function()
            awful.client.focus.byidx(1)
            if client.focus then client.focus:raise() end
        end
    ),
    awful.key({modkey}, "k",
        function()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end
    ),
    awful.key({modkey}, "w",
        function()
            mywibox[mouse.screen].visible = not mywibox[mouse.screen].visible
        end
    ),
    awful.key({modkey, "Shift"}, "j",
        function()
            awful.client.swap.byidx(1)
        end
    ),
    awful.key({modkey, "Shift"}, "k",
        function()
            awful.client.swap.byidx( -1)
        end
    ),
    awful.key({modkey, "Control"}, "j",
        function()
            awful.screen.focus_relative( 1)
        end
    ),
    awful.key({modkey, "Control"}, "k",
        function()
            awful.screen.focus_relative(-1)
        end
    ),
    awful.key({"Mod1"}, "Tab",
        function()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end
    ),
    awful.key({modkey}, "Return",
        function()
            awful.util.spawn(terminal)
        end
    ),
    awful.key({modkey}, ";",
        function()
            awful.tag.incmwfact(0.05)
        end
    ),
    awful.key({modkey}, "r",
        function()
            mypromptbox[mouse.screen]:run()
        end
    ),
    awful.key({modkey}, "h",
        function()
            awful.tag.incmwfact(-0.05)
        end
    ),
    awful.key({modkey, "Shift"}, "h",
        function()
            awful.tag.incnmaster(1)
        end
    ),
    awful.key({modkey, "Shift"}, ";",
        function()
            awful.tag.incnmaster(-1)
        end
    ),
    awful.key({modkey, "Control"}, "h",
        function()
            awful.tag.incncol(1)
        end
    ),
    awful.key({modkey, "Control"}, "l",
        function()
            awful.tag.incncol(-1)
        end
    ),
    awful.key({modkey, "Shift"}, "space",
        function()
            awful.layout.inc(layouts, -1)
        end
    )
)

-- Section: Keyboard shortcuts -> Client keys
clientkeys = awful.util.table.join(
    awful.key({modkey}, "f",
        function(c)
            c.fullscreen = not c.fullscreen
        end
    ),
    awful.key({modkey}, "x",
        function(c)
            c:kill()
        end
    ),
    awful.key({modkey, "Control"}, "Return",
        function(c)
            c:swap(awful.client.getmaster())
        end
    ),
    awful.key({modkey, "Shift"}, "r",
        function(c)
            c:redraw()
        end
    ),
    awful.key({modkey}, "t",
        function(c)
            c.ontop = not c.ontop
        end
    ),
    awful.key({modkey}, "n",
        function(c)
            c.minimized = not c.minimized
        end
    ),
    awful.key({modkey}, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical = not c.maximized_vertical
        end
    )
)

-- Section: Keyboard shortcuts -> Tag and screen keys
keynumber = 0
for s = 1, screen.count() do
    keynumber = math.min(9, math.max(#tags[s], keynumber));
end
for i = 1, keynumber do
    globalkeys = awful.util.table.join(
        globalkeys,
        awful.key({modkey}, "#" .. i + 9,
            function()
                local screen = mouse.screen
                if tags[screen][i] then
                    awful.tag.viewonly(tags[screen][i])
                end
            end
        ),
        awful.key({modkey, "Control"}, "#" .. i + 9,
            function()
                local screen = mouse.screen
                if tags[screen][i] then
                    awful.tag.viewtoggle(tags[screen][i])
                end
            end
        ),
        awful.key({modkey, "Shift"}, "#" .. i + 9,
            function()
                if client.focus and tags[client.focus.screen][i] then
                    awful.client.movetotag(tags[client.focus.screen][i])
                end
            end
        ),
        awful.key({modkey, "Control", "Shift"}, "#" .. i + 9,
            function()
                if client.focus and tags[client.focus.screen][i] then
                    awful.client.toggletag(tags[client.focus.screen][i])
                end
            end
        )
    )
end
clientbuttons = awful.util.table.join(
    awful.button({},       1, function(c) client.focus = c; c:raise() end),
    awful.button({modkey}, 1, awful.mouse.client.move),
    awful.button({modkey}, 3, awful.mouse.client.resize)
)
root.keys(globalkeys)

-- Section: Window rules
awful.rules.rules = {
    {rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = true,
            opacity = 1.0,
            keys = clientkeys,
            maximized_horizontal = true,
            maximized_vertical = true,
            floating = true,
            buttons = clientbuttons
        }
    },
    {rule = {class = "XTerm"},
        properties = {
            tag = tags[1][1] -- tag 1 (console) of screen 1
        }
    },
    {rule = {class = "MPlayer"},
        properties = {
            tag = tags[1][4] -- tag 4 (media) of screen 1
        }
    },
    {rule = {class = "pinentry"},
        properties = {
            floating = true
        }
    },
    {rule = {class = "gimp"},
        properties = {
            floating = true
        }
    },
}

-- Section: Signals
client.add_signal("manage",
    function(c, startup)
        if not startup
           and not c.size_hints.user_position
           and not c.size_hints.program_position
        then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
)
client.add_signal("focus",
    function(c)
        c.border_color = beautiful.border_focus
        c.opacity = 1.0
    end
)
client.add_signal("unfocus",
    function(c)
        c.border_color = beautiful.border_normal
        c.opacity = 0.6
    end
)
